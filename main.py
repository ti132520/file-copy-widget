import os
import shutil
import json
json_path = ".\\data.json"

def copy_file(is_json_setting, src_path='', dst_path='', file_type='mp4'):
    if is_json_setting == "y":
        if os.path.exists(json_path):
            json_data = json.load(open(json_path))
            src_path = json_data["src_path"]
            dst_path = json_data["dst_path"]
        else:
            print("json文件不存在")
            return
        json_data = {
            "src_path": src_path,
            "dst_path": dst_path,
            "file_type": json_data["file_type"],
            "file_data": json_data["file_data"]
        }
    else:
        json_data = {
            "src_path": src_path,
            "dst_path": dst_path,
            "file_type": file_type,
            "file_data": []
        }
    if not file_type:
        file_type = "mp4"

    if not os.path.exists(src_path):
        print("原始目录不存在")
        return
    if not os.path.exists(dst_path):
        os.makedirs(dst_path)

    for root, dirs, files in os.walk(src_path):
        j = 1
        files_count = len(files)
        for file in files:
            print("当前文件夹：" + root)
            suffix = os.path.splitext(file)[-1]
            if suffix == '.' + file_type:
                print("当前文件：" + file)
                print("当前第" + str(j) + "个文件，共" + str(files_count) + "个文件")
                j = j + 1
                if file not in json_data['file_data']:
                    print("复制中。")
                    json_data['file_data'].append(file)
                    shutil.copy(os.path.join(root, file), dst_path + '\\' + file)
                else:
                    print("已存在，跳过。")

    with open(json_path, 'w') as f:
        json.dump(json_data, f)


if __name__ == "__main__":
    print("文件批量复制工具 v 1.0")
    is_json_setting = input("是否使用上次json配置文件？(y/n)")
    if is_json_setting == "y":
        copy_file(is_json_setting)
    else:
        while(True):
            src_path = input("输入目标文件路径：")
            dst_path = input("输入文件复制路径：")
            file_type = input("输入复制文件类型：(默认mp4)")
            if src_path and dst_path:
                copy_file(is_json_setting, src_path, dst_path, file_type)
                break
input('Press Enter to exit...')